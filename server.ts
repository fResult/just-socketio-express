import 'dotenv/config'
import express from 'express'
import cors from 'cors'
import { Server, Socket } from 'socket.io'
import { createServer } from 'http'

const server = express()
const http = createServer(server)
const io = new Server(http, {})

server.use(cors())
server.use(express.static('public'))

server.get('/', (req, res) => {
  res.sendFile(`${__dirname}/views/index.html`)
  // res.json({ message: 'hello world' })
})

io.on('connection', function(socket: Socket) {
  console.log(`Socket ${socket.id} is now connected.`)
  socket.on('disconnect', () => {
    console.log(`${socket.id} is disconnected.`)
  })
})

const listener = http.listen(process.env.PORT, () =>
  console.log(`Server is starting at port ${process.env.PORT}`)
)

// // Require the fastify framework and instantiate it
// const ftf = fastify({ logger: false })
//
// // Setup our static files
// ftf.register(fastifyStatic, {
//   root: path.join(__dirname, 'public'),
//   prefix: '/' // optional: default '/'
// })
//
// // fastify-formbody lets us parse incoming forms
// ftf.register(fastifyFormBody)
//
// // point-of-view is a templating manager for fastify
// ftf.register(pointOfView, {
//   engine: {
//     handlebars: handlebars
//   }
// })
